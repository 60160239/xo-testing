//import java.util.Arrays; For testing purpose
import java.util.Scanner;

public class App {
    public static void main(String[] args) throws Exception {
        xoBoard newBoard = new xoBoard();
        newBoard.thisBoard = boardInput();
        newBoard.tellWin();
    }

    private static int[][] boardInput(){
        int[][] xoboard = new int[3][3];  
        Scanner sc=new Scanner(System.in);
        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                int n=sc.nextInt();
                xoboard[i][j] = n;
            }
        } 
        //System.out.println(Arrays.deepToString(xoboard).replace("], ", "]\n")); For testing purpose
        return xoboard;
    }
    
}
