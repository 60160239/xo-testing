public class xoBoard {
  public int[][] thisBoard;
  private int scoreX =0;
  private int scoreO =0;
  private int blank =0;

  private void checkRowCondition(int[][] board){
    for(int i=0;i<3;i++){
      if(board[i][0]==board[i][1]&&board[i][1]==board[i][2]){
          if(board[i][0]==1){
            scoreX++;
          }else if(board[i][0]==2){
            scoreO++;
          }else{
            blank++;
          }
      }
    }
  }

  private void checkColumnCondition(int[][] board){
    for(int i=0;i<3;i++){
      if(board[0][i]==board[1][i]&&board[1][i]==board[2][i]){
          if(board[0][i]==1){
            scoreX++;
          }else if(board[0][i]==2){
            scoreO++;
          }else{
            blank++;
          }
      }
    }
  }

  private void checkCrossCondition(int[][] board){
    if(board[0][0]==board[1][1]&&board[1][1]==board[2][2]||board[0][2]==board[1][1]&&board[1][1]==board[2][0]){
      if(board[1][1]==1){
        scoreX++;
      }else if(board[1][1]==2){
        scoreO++;
      }else{
        blank++;
      }
    }
  }

  private void checkIfBlank(int[][] board){
    for(int i=0;i<3;i++){
      for(int j=0;j<3;j++){
        if(board[i][j]==0){
          blank++;
        }
      }
    }
  }

  private void checkOverallCondition(){
      checkColumnCondition(thisBoard);
      checkCrossCondition(thisBoard);
      checkRowCondition(thisBoard);
      checkIfBlank(thisBoard);
  }

    public void tellWin(){
        checkOverallCondition();
        if(blank>0){
          System.out.println("Not finish yet");
        }else if(scoreX>scoreO){
          System.out.println("X Win");
        }else if(scoreO>scoreX){
          System.out.println("O Win");
        }else if(scoreO==scoreX){
          System.out.println("Draw");
        }
        resetScore();
    }

    private void resetScore(){
      scoreX =0;
      scoreO =0;
      blank =0;
    }
}
